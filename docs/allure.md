# Terminale NSI au lycée Henri Avril



## Les thèmes du programme
Le programme est découpé en 5 parties :  

- Structures de données
- Bases de données
- Réseaux et système d'exploitation
- Programmation
- Algorithmie

.
## Administration de la base via formulaire php

```python
for i in range(10):
    print("Salut à tous !")
```
```html
    <h1> ldmfkhsmudfj </h1>
```

```css
html, body {
    margin: 0;
    padding: 0;
}
body {
 background-color: white; 
 font-family: Verdana, sans-serif; 
 font-size: 100%;
 }
h1 {
 font-size: 200%; 
 color: navy; 
 text-align: center;
}
```




```JS
var year = 2016; // des chiffres
var president = "François H"; // des mots
var candidats = ["Pierre", "Paul", "Jacques"]; // une liste de noms
candidats[0]; // "Pierre"
candidats[1]; // "Paul"
var notes = [12,18,20]; // une liste de notes
notes[0]; // 12
```

```c
#include<stdio.h>
int main(){
    int i;
    for(i = 0; i < 10; i++){
        printf("Salut à tous !\n");
    }
    return 0;
}
```

```SQL
SELECT * FROM client;
```

>definiton
>toutes les lignes
>commencent par un chevron

!!! note "note"
    une note

!!! summary "summary"
    summary

!!! example "example"
    example

!!! important "important"
    Important

!!! info "info"
    info

!!! fail "fail"
    fail

!!! attention "attention"
    attention

!!! danger "danger"
    danger

!!! bug "bug"
    bug

!!! faq "faq"
    et pourquoi pas
!!! done "done"
    done

!!! cite "cite"
    cite

[un bouton creux](https://console.basthon.fr/){ .md-button }

[Un bouton pleins](https://console.basthon.fr/){ .md-button .md-button--primary }  

![texte alterntif](chemin de l'image){align=left}

[lien](adresse)

<form action="../" onsubmit="return checkCheckBoxes(this);">
    <p><input type="CHECKBOX" name="CHECKBOX_1" value="This..."> This...</p>
    <p><input type="CHECKBOX" name="CHECKBOX_2" value="That..."> That...</p>
    <p><input type="CHECKBOX" name="CHECKBOX_3" value="...and The Other"> ...and The Other</p>
    <p><input type="SUBMIT" value="Submit!"></p>
</form>

<script type="text/javascript" language="JavaScript">
<!--
function checkCheckBoxes(theForm) {
    if (
    theForm.CHECKBOX_1.checked == false &&
    theForm.CHECKBOX_2.checked == false &&
    theForm.CHECKBOX_3.checked == false) 
    {
        alert ('You didn\'t choose any of the checkboxes!');
        return false;
    } else {    
        return true;
    }
}
//-->
</script> 

!!! abstract "QCM à cocher"
    On souhaite proposer un QCM, auto corrigé.

!!! done "Exemple 1"
    Pour cet exercice sur Python,
     on ne regarde que **si l'identifiant est valide**,
     il pourrait être mal choisi.

    === "Cocher les identifiants valides"
        - [ ] `as`
        - [ ] `Roi`
        - [ ] `2ame`
        - [ ] `v413t`
        - [ ] `dix`
        - [ ] `n'œuf`
        - [ ] `huit`
        - [ ] `Sète`
        - [ ] `carte_six`
        - [ ] `_5`
        - [ ] `%4`
        - [ ] `quatre-moins-un`
        - [ ] `2!`
        - [ ] `_`

    === "Solution"
        - ❌ `as` ; c'est un mot réservé.
        - ✅ `Roi`
        - ❌ `2ame` ; interdit de commencer par un chiffre.
        - ✅ `v413t`
        - ✅ `dix`
        - ❌ `n'œuf` ; interdit d'utiliser `'`
        - ✅ `huit`
        - ✅ `Sète`
        - ✅ `carte_six`
        - ✅ `_5`
        - ❌ `%4` ; interdit d'utiliser `%`
        - ❌ `quatre-moins-un` ; interdit d'utiliser `-`
        - ❌ `2!` ; interdit d'utiliser `!`
        - ✅ `_`


!!! faq "test"
    === "Cocher les identifiants valides"
        - [ ] `as`
        - [ ] `Roi`
        - [ ] `2ame`
        - [ ] `v413t`
        - [ ] `dix`
        - [ ] `n'œuf`
        - [ ] `huit`
        - [ ] `Sète`
        - [ ] `carte_six`
        - [ ] `_5`
        - [ ] `%4`
        - [ ] `quatre-moins-un`
        - [ ] `2!`
        - [ ] `_`

    === "Solution"
        - ❌ `as` ; c'est un mot réservé.
        - ✅ `Roi`
        - ❌ `2ame` ; interdit de commencer par un chiffre.
        - ✅ `v413t`
        - ✅ `dix`
        - ❌ `n'œuf` ; interdit d'utiliser `'`
        - ✅ `huit`
        - ✅ `Sète`
        - ✅ `carte_six`
        - ✅ `_5`
        - ✅ `_`
        - ❌ `%4` ; interdit d'utiliser `%`
        - ❌ `quatre-moins-un` ; interdit d'utiliser `-`
        - ❌ `2!` ; interdit d'utiliser `!`
        - ✅ `_`
